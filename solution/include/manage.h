#include <stdio.h>

FILE* openReadFile(const char* path);
FILE* openWriteFile(const char* path);
void closeFile(FILE* file);
