#include <stdint.h>
#include <stdio.h>

#pragma once
struct image {
    uint64_t width, height;
    struct pixel *data;
};
struct image rotate(struct image const img);

struct pixel {
    uint8_t b, g, r;
};

struct image* create_img(struct image *img, size_t width, size_t height);
struct image imageNewStruct(const struct image* img);
void destroy(struct image img);
