#include "image.h"

#include <stdint.h>
#include <stdio.h>

#pragma once
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_ERROR_HEADERS
};

enum write_status write_bmp(FILE *out, struct image *img);

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_ERROR,
    INVALID_SIZE
};

enum read_status read_bmp(FILE *in, struct image *img);
