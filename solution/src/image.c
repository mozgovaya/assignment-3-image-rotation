#include "../include/image.h"

#include <stdlib.h>

void destroy(struct image img) {
    free(img.data);

}

struct image* create_img(struct image* img, uint64_t width, uint64_t height){
    img->height = height;
    img->width = width;
    img->data = malloc (width * height * sizeof (struct pixel));
    return img;
}

struct image imageNewStruct(const struct image* img) {
    return (struct image) {
            .width = img->height,
            .height = img->width,
            .data = malloc(img->height * img->width * sizeof(struct pixel))
    };
}
uint64_t calculateNewAddress(const struct image* img, uint64_t i, uint64_t j) {
    uint64_t address = img->height - i - 1 + j * img->height;
    return address;
}

struct image rotate(struct image const img) {
    struct image newImg = imageNewStruct(&img);

    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            newImg.data[calculateNewAddress(&img,i,j)]=img.data[i * img.width + j];
        }
    }
    return newImg;
}

