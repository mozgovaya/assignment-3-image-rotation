#include "file.h"
#include "manage.h"
#include <stdio.h>
void info(const char *mes) {
    fputs(mes, stdout);
}

int error(const char *mes) {
    fputs(mes, stderr);
    return 0;
}

int main(int argc, char **argv) {
    if (argc != 3) {
        return error("Необходимо 2 параметра\n");
    }

    FILE *in = openReadFile(argv[1]);
    if (in == NULL) {
        return error("Не удалось открыть входной файл\n");
    }

    struct image img = {0};
    enum read_status rs = read_bmp(in, &img);
    if (rs != READ_OK) {
        destroy(img);
        closeFile(in);
        return error("Неверного формат bmp\n");
    }
    closeFile(in);

    info("Входной файл успешно прочитан\n");

    struct image img_rotate = rotate(img);
    destroy(img);
    FILE *out = openWriteFile(argv[2]);
    if (out == NULL) {
        return error("Не удалось открыть выходной файл\n");
    }

    enum write_status ws = write_bmp(out, &img_rotate);
    if (ws != WRITE_OK) {
        destroy(img_rotate);
        closeFile(out);
        return error("Ошибка конвертации в .bmp\n");
    }
    destroy(img_rotate);
    closeFile(out);

    info("Картинка перевёрнута\n");
    return 0;
}


